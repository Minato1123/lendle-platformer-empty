/*
 * 定義一個 scene，用成員變數儲存 scene 上面的物件
 * override preload, create, update 函式
 */
class Scene extends Phaser.Scene {
  /*********************************************************************************************************************** */
  constructor() {
    super();
  }

  /*********************************************************************************************************************** */
  preload() {
    this.load.image(
      "sky",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fsky.png?v=1583115110427"
    );
    this.load.image(
      "ground",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fplatform.png?v=1583115124057"
    );
    this.load.image(
      "star",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fstar.png?v=1583115116442"
    );
    this.load.image(
      "bomb",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fbomb.png?v=1583115128502"
    );
    this.load.spritesheet(
      "dude",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fdude.png?v=1583115132173",
      {frameWidth: 32, frameHeight: 48}
    );
  }

  /*********************************************************************************************************************** */
  create() {
    this.add.image(400, 300, "sky")
    this.platforms = this.physics.add.staticGroup()
    this.platforms.create(600, 400, "ground")
    this.platforms.create(50, 250, "ground")
    this.platforms.create(750, 220, "ground")

    this.platforms.create(400, 568, "ground").setScale(2).refreshBody()
    // 也可以這樣打
    // this.floor = this.platforms.create(400, 568, "ground")
    // this.floor.setScale(2)
    // this.floor.refreshBody()

    this.player = new Player(this, 400, 420)
    this.physics.add.collider(this.player, this.platforms)

    this.cursors = this.input.keyboard.createCursorKeys()

    this.anims.create({
      key: "right",
      frames: this.anims.generateFrameNumbers("dude", {start: 5, end: 8}),
      frameRate: 10,
      repeat: -1
    })
    this.anims.create({
      key: "left",
      frames: this.anims.generateFrameNumbers("dude", {start: 0, end: 3}),
      frameRate: 10,
      repeat: -1
    })
    this.anims.create({
      key: "stop",
      frames: this.anims.generateFrameNumbers("dude", {start: 4, end: 4}),
      frameRate: 10,
      repeat: -1
    })

    this.stars = this.physics.add.group()
    for (let i = 10; i < 800; i+=20) {
      let star = new Star(this, i, 20)
      this.stars.add(star)
      // star.setBound(0.5)
      // star.body.setCircle(12)

    }
    this.stars.children.iterate((s) => {
      s.setBounce(0.7)
      s.body.setCircle(12)
      s.setCollideWorldBounds(true);
    })
    this.physics.add.collider(this.stars, this.platforms)
    this.physics.add.collider(this.stars, this.stars)
    this.physics.add.overlap(this.player, this.stars, this.collectStar, null, this)
    this.scoreText = this.add.text(16, 16, "score: 0", {
      fontSize: "25px",
      fill: "#000"
    })
    this.timeText = this.add.text(635, 16, "time: 0 s", {
      fontSize: "25px",
      fill: "#000"
    })
    this.startTime = new Date().getTime()

  }

  collectStar(player, star) {
    player.setScore(player.getScore() +1 )
    star.disableBody(true, true)
  }

  /*********************************************************************************************************************** */
  update() {
    this.scoreText.setText("score: " + this.player.getScore())
    let currentTime = new Date().getTime()
    let deltaTime = Math.ceil((currentTime - this.startTime) / 1000)
    if (this.player.getScore() < 40) {
      this.timeText.setText("time: " + deltaTime + " s")
    }
    this.player.idle()
    if (this.cursors.left.isDown) {
      this.player.goLeft()
    } else if (this.cursors.right.isDown) {
      this.player.goRight()
    }

    if (this.cursors.up.isDown && this.player.body.touching.down) {
      this.player.goJump()
    }
    if (this.cursors.space.isDown && this.player.body.touching.down) {
      this.player.goJump()
    }
    if (this.cursors.down.isDown) {
      this.player.goDown()
    }
    this.player.update()
  }
}
