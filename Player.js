class Player extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y) {
    super(scene, x, y, "dude");
    this.scene = scene;
    scene.physics.world.enable(this);
    scene.add.existing(this);
    this.setCollideWorldBounds(true);
    this.left=false;
    this.right=false;
    this.jump=false;
    this.down=false;
    this.score=0;
  }
  
  getScore(){
    return this.score;
  }
  
  setScore(s){
    this.score=s;
  }
  
  goLeft(){
    this.left=true;
  }
  
  goRight(){
    this.right=true;
  }
  
  goJump(){
    this.jump=true;
  }

  goDown() {
    this.down=true;
  }
  
  idle(){
    this.left=false;
    this.right=false;
    this.jump=false;
    this.down=false;

  }
  
  update(){
    if(this.left) {
      this.setVelocityX(-160)
      this.anims.play("left", true)
    } else if (this.right) {
      this.setVelocityX(160)
      this.anims.play("right", true)
    } else {
      this.setVelocityX(0)
      this.anims.play("stop", true)

    }
    if (this.jump) {
      this.setVelocityY(-330)
    } else if (this.down) {
      this.setVelocityY(300)
    }
  }
}